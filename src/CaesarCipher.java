import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CaesarCipher {

    private static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private static String help = "You can use spaces in your MESSAGE, but numbers and symbols are restricted.\r\n" +
                                 "KEY is a number, not text.";
    private static String enterMessage = "Enter your MESSAGE:";
    private static String enterKey = "Enter your KEY(alphabet shift):";
    private static int space = 32;


    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
            System.out.println(help);
            System.out.println(enterMessage);
            String message = reader.readLine();
            while(!isMessageCorrect(message)){
                System.out.println("Incorrect MESSAGE input");
                System.out.println(help);
                System.out.println(enterMessage);
                message = reader.readLine();
            }
            System.out.println(enterKey);
            String key = reader.readLine();
            while(!isKeyCorrect(key)){
                System.out.println("Incorrect KEY input");
                System.out.println(help);
                System.out.println(enterKey);
                key = reader.readLine();
            }
            Integer intKey = Integer.parseInt(key);
            String encryptionResult = encrypt(message, intKey);
            String decryptionResult = decrypt(encryptionResult, intKey);
            System.out.println("Result of encryption: " + encryptionResult);
            System.out.println("Result of decryption: " + decryptionResult);
        } catch (NumberFormatException | IOException e){
            System.out.println("Caught exception with message: " + e.getMessage());
        }
    }

    private static String encrypt(String text, Integer key){
        String message = text.toLowerCase();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < message.length(); ++i){
            if (message.charAt(i) == space) {
                stringBuilder.append((char)space);
                continue;
            }
            stringBuilder.append(alphabet.charAt((message.charAt(i) - alphabet.charAt(0) + key) % alphabet.length()));
        }
        return stringBuilder.toString();
    }

    private static String decrypt(String text, Integer key){
        String message = text.toLowerCase();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < message.length(); ++i){
            if (message.charAt(i) == space) {
                stringBuilder.append((char)space);
                continue;
            }
            stringBuilder.append(alphabet.charAt((message.charAt(i) - alphabet.charAt(0) - key + alphabet.length()) % alphabet.length()));
        }
        return stringBuilder.toString();
    }

    private static boolean isMessageCorrect(String text){
        String message = text.toLowerCase();
        for (int i = 0; i < message.length(); ++i){
            if (message.charAt(i) == space) continue;
            if (message.charAt(i) < alphabet.charAt(0) || message.charAt(i) > alphabet.charAt(alphabet.length()-1)){
                return false;
            }
        }
        return true;
    }

    private static boolean isKeyCorrect(String key){
        boolean result = true;
        try {
            Integer intKey = Integer.parseInt(key);
        } catch (NumberFormatException e){
            result = false;
        }
        return result;
    }
}
